
package pdf_to_html_converter;

use strict; 
use warnings;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
use autodie; # die if problem reading or writing a file

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();


use Try::Tiny;
use Getopt::Long;
use Term::ReadKey;
use File::Basename;
use File::Spec;
use IPC::Cmd qw[can_run run run_forked];
use Log::Log4perl qw(get_logger :levels);
use HTML::TreeBuilder 5 -weak; # Ensure weak references in use
use HTML::Element;
use HTML::Tidy;
use File::Copy qw(move);

require './perl_DB/db_log_DBI.pl';

my $log_f;
#overwrite old log file 
open (OW, ">C:\\slidefish\\pdf_to_html\\pdf_to_html.log");
close(OW);
$log_f = get_logger("pdftohtmlLog.info");
$log_f->debug('pdf to html Logging initialised, yay!');
$log_f->level($DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL
#$log_f->level($INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL

my($debug, $files_in_dir, $i);
my($volume, $input_dir, $processing_dir, $error_dir, $out_dir);
my($key);

$debug = 1;
# $debug = 0;
$files_in_dir = 0;
$i = -1;
$volume = "C:";
$input_dir = "/slidefish/pdf_to_html/input";
$processing_dir = "/slidefish/pdf_to_html/processing";
$error_dir = "/slidefish/pdf_to_html/error";
$out_dir = 'C:/slidefish/pdf_to_html/output';

$log_f->debug( sprintf("Main: input_dir: %s", $input_dir) );

############ Main loop using change notify
use Win32::ChangeNotify;

my $watch_dir ||= '.';
my $watch_sub_tree =  0;

$watch_dir = 'C:/slidefish/pdf_to_html/input',

my $notify = Win32::ChangeNotify->new( $watch_dir, $watch_sub_tree, 'FILE_NAME' );
printf "Notify = %s\n", $notify;
my $files = {};
$files = ProcessDir( $files );
while( 1 && (not defined ($key = ReadKey(-1)))) {

	$i++;
	my $Result = $notify->wait( 5 * 1000 ); # Check every 10 seconds
	if($Result) 
	{

		print 'Something changed', "\n";
		$notify->reset;
		$files = ProcessDir( $files );
		$i = 0;
		
	}
	else 
	{
		printf("no file loop: %s\n", $i);
	}
}

printf("\npress enter key to exit...\n");
ReadLine(0);

sub ProcessDir { #DirMon.pl
  my( $OrigList ) = @_;
  my $NewList = {};
  my $Time = scalar localtime();

  GetList( $watch_dir, $NewList );
  printf("ProcessDir: entry\nOrigList: $OrigList\n");
  print keys (%$OrigList), "\n";
  printf("ProcessDir: entry: Newlist: \n");
  print keys (%$NewList), "\n";
  # First check for additions...
  print "Check for additions\n";
  foreach my $File ( ReportNewFiles( $OrigList, $NewList ) ) {
	$log_f->debug( sprintf("$Time: File '$File->{name}' added \n") );
    if ($File->{name} =~ /\.pdf$/) {

		my $in_file = $watch_dir .'/'. $File->{name};  

		# run the command line tool to convert the file from pdf to html
		my ($html_dir, $short_html_dir);
		($html_dir = $in_file) =~ s/\.[Pp][Dd][Ff]$//;
		($short_html_dir = $File->{name}) =~ s/\.[Pp][Dd][Ff]$//;
		# unlink glob "$html_dir/sb*.htm";
		# unlink glob "$html_dir/sb*.png";

		$log_f->debug( sprintf("in_path: %s; html_dir: %s", $in_file, $html_dir) );
		
		my $pdf2html_cmd = "C:\\slidefish\\verypdf\\pdf2html_cmd\\pdf2html.exe";
		$pdf2html_cmd .= " -imgformat 1 -oneword -noseo -noutf8 -notextinmeta -notxtidx";
		$pdf2html_cmd .= " $in_file $html_dir" . '.html';
		
		$log_f->debug( sprintf("process_input_file::pdf2html_cmd: (%s)", $pdf2html_cmd) );
		my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf );
		$success = $error_message = $full_buf = $stdout_buf = $stderr_buf = '';
		( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
				run( command => $pdf2html_cmd, verbose => 1 );
		(length $success) && $log_f->debug( sprintf("process_input_file::pdf2html_cmd output success: (%s)", $success) );
		(length $error_message) && $log_f->debug( sprintf("pdf2html_cmd output error_message: (%s)", $error_message) );
		my $tmp = join(", ", @$full_buf);
		# $log_f->debug( sprintf("pdf2html_cmd output full_buf: (%s)", $tmp) );
		$tmp = join(", ", @$stdout_buf);
		$log_f->debug( sprintf("pdf2html_cmd output stdout_buf: (%s)", $tmp) );
		$tmp = join(", ", @$stderr_buf);
		$log_f->debug( sprintf("pdf2html_cmd output stderr_buf: (%s)", $tmp) );

		$log_f->info( sprintf("pdf2html_cmd: postprocessing") );
		if(! -d $html_dir) {
			$log_f->error( sprintf("process_input_file::pdf2html_cmd: ERROR html_dir (%s) does not exist", $html_dir) );
		}
		
		clean_up_verypdf_html_dir($html_dir);
		
		# move the original pdf file into the html directory
		# and move the html directory into the output directory
		
		$log_f->debug( sprintf("move: File $in_file  to $html_dir.\n") );
		move($in_file , $html_dir) || die $!; 
		$log_f->debug( sprintf("move: directory $html_dir to $out_dir.\n") );
		move($html_dir, File::Spec->catdir( $out_dir, $short_html_dir)) || die $!;
		
		
	} else { 
		$log_f->info( sprintf("$Time: File '$File->{name}' is not a pdf file \n") );
	}
  }
  # Now check for deleted file 
  foreach my $File ( ReportRemovedFiles( $OrigList, $NewList ) ) {
	$log_f->info( sprintf("$Time: File '$File->{name}' removed.\n") );
    print "$Time: File '$File->{name}' removed.\n";
  }

  return( $NewList );
  
}

sub clean_up_verypdf_html_dir
{
	my $dir = $_[0];
	$log_f->debug( sprintf("Directory: $dir \n") );
	# clean up the output from the verypdf processing
	# remove unwanted files
	unlink $dir . "/index.htm";
	unlink $dir . "/pg_index.htm";	
	unlink $dir . "/pg_bmark.htm";
	unlink $dir . "/pg_nav.htm";
	unlink glob "$dir/*.gif";
	
	# clean up the html files
	if( opendir( DIR, $dir ) ) 
	{
		my @Files;
		my @Dirs;
		while( my $File = readdir( DIR ) ) 
		{
			if( $File !~ /pg[0-9]+\.htm$/ ) 
			{
				$log_f->debug( sprintf("htm file $File\n") );
				push( @Files, $File ) if( -f "$dir/$File" );
			}
		}
		closedir( DIR );
		foreach my $File ( @Files ) 
		{
			my $f = "$dir/$File";
			clean_html($f);
		}
    }
}

sub clean_html
{
	my $f = $_[0];
	my ($filename, $directory, $suffix) = fileparse($f, qr/\.[^.]*/);
	my $new_f = $directory . 'sb_' . $filename . $suffix;
	$log_f->debug( sprintf("new file name  \n%s\n\n", $new_f) );
	
	my $tree = HTML::TreeBuilder->new; # empty tree
	$tree->no_space_compacting(1);
	$tree->parse_file($f);
	# html_recurse($tree, 0); # prints it out
	# $log_f->debug( sprintf("CLEAN THIS MF! \n%s\n\n", $tree->as_HTML) );
	# $log_f->debug( sprintf("CLEAN THIS MF! \n%s\n\n", $tree->as_text) );
	
	my $el;
	my @elements = $tree->elementify(); #https://stackoverflow.com/questions/2345884/how-would-i-remove-all-script-tags-and-everything-in-between-from-multiple-f
	@elements = $tree->look_down(_tag => 'script');
	while( my $element = shift(@elements) ) 
	{
		$element->delete();
	}
	
	@elements = $tree->find_by_tag_name('title');
	while( my $element = shift(@elements) ) 
	{
		#printf("Found %s:, value: %s\n", $element->tag(), join(',', $element->content_list()));
		
		$element->objectify_text();
		my @childNodes = $element->content_list();
		for(my $j=0; $j < scalar(@childNodes); $j++) {
		   my $childNode = $childNodes[$j];
		   if (ref($childNode) && ($childNode->tag() eq '~text')) { #TEXTNODE
			  my $newElement = HTML::Element->new('~text', 'text' => 'Slidefish HTML5 conversion');
			  $childNode->replace_with($newElement)->delete();
		   }
		}
		$element->deobjectify_text();
	    #printf("Found %s:, NEW value: %s\n", $element->tag(), join(',', $element->as_text()));
	}
	
	@elements = $tree->find_by_tag_name('meta');
	while( my $element = shift(@elements) ) 
	{
		#my @children = $element->content_list();
		#printf("element %s, Found tag %s:, content: \n", $element->as_HTML(), $element->tag());
		$element->delete();

	}
	
	my $output = IO::File->new($new_f, 'w');
	$output->binmode(':raw:utf8');
	print $output HTML::Tidy->new( { wrap => 90,
									 indent => 'auto',
									 'wrap-attributes' => 'yes',
								   }
								 )->clean($tree->as_HTML(undef, "  "));
	$output->close();
	$tree = $tree->delete;
}

sub html_recurse  #http://www.lemoda.net/perl/html-treebuilder-basic/
{
    my ($node, $depth) = @_;

    # Print indentation according to the level of recursion.

    my $space = "  " x $depth;
	
    # If $node is a reference, then it is an HTML::Element.

    if (ref $node) 
	{

        # Print the tag associated with $node, for example "html" or
        # "li".

		$log_f->debug( sprintf("%s%s", $space, $node->tag ()));

        # $node->content_list () returns a list of child nodes of
        # $node, which we store in @children.

        my @children = $node->content_list ();
        for my $child_node (@children) 
		{
            html_recurse ($child_node, $depth + 1);
        }
    }
    else 
	{

        # If $node is not a reference, then it is just a piece of text
        # from the HTML file.

		$log_f->debug( sprintf("%s%s", $space, substr($node, 0, 200)));
    }
}
sub ReportNewFiles {    # FROM DirMon.pl
  my( $Old, $New ) = @_;
  my $TotalOld = scalar ( keys( %$Old ) );
  my @NewFiles;
  
  # print "ReportNewFiles: entry\n";
  foreach my $NewName ( sort( keys( %$New ) ) ) {
	# printf("in ReportNewFiles: NewName = %s\n", $NewName);
    my $iCount = $TotalOld;
    foreach my $OldName ( keys( %$Old ) ) {
		# printf("in ReportNewFiles: OldName = %s\n", $OldName);
      last if( lc $NewName eq lc $OldName );
      $iCount--;
    }
    push( @NewFiles, $New->{$NewName} ) if( 0 == $iCount );
  }
  # print "ReportNewFiles: exit\n";  
  return( @NewFiles );
}


sub ReportRemovedFiles { # FROM DirMon.pl
  my( $Old, $New ) = @_;
  my $TotalNew = scalar ( keys( %$New ) );
  # print "ReportRemovedFiles: entry\n";
  my @RemovedFiles;
  foreach my $OldName ( sort( keys( %$Old ) ) ) {
    my $iCount = $TotalNew;
    foreach my $NewName ( keys( %$New ) ) {
      last if( lc $NewName eq lc $OldName );
      $iCount--;
    }
    push( @RemovedFiles, $Old->{$OldName} ) if( 0 == $iCount );
  }
  # print "ReportRemovedFiles: exit\n";
  return( @RemovedFiles );
}



sub GetList {   # FROM DirMon.pl
  my( $Dir, $List ) = @_;
  if( opendir( DIR, $Dir ) ) {
    my @Files;
    my @Dirs;
    while( my $File = readdir( DIR ) ) {
      next if( ( "." eq $File ) || ( ".." eq $File ) );
      push( @Files, $File ) if( -f "$Dir/$File" );
      push( @Dirs, $File ) if( -d "$Dir/$File" );
    }
    closedir( DIR );
    foreach my $File ( @Files ) {
      my %Entry;
      # my $Perm;
      # my @Stats;
      $Entry{name} = $File;
      # @Stats = stat( "$Dir/$File" );
      # $Entry{size} = $Stats[7];
      # $Entry{date} = $Stats[10];
      $List->{lc $File} = \%Entry;
    }
    if( $watch_sub_tree ) {
      foreach my $SubDir ( @Dirs ) {
        GetList( "$Dir/$SubDir", $List );
      }
    }
  }
}

sub error_handler
{
	
}

#############################################
#############################################

1;
