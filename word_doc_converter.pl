#
# C:\slidefish\xls_converter.pl --test --debug
# --volume "C:"
# --indir C:\slidefish\input
# --outdir C:\slidefish\output
# --infilen Title1.xls
# --pdf title1.pdf 

use strict; use warnings;
use Try::Tiny;
use Getopt::Long;
use File::Spec;

require './perl_DB/db_log_DBI.pl';

my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL
#$log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL
if($Log::Log4perl::INFO || $Log::Log4perl::DEBUG) {} #squash warnings

use Win32::OLE qw(in with);
use Win32::OLE::Const qw( Microsoft.Word );
use Win32::OLE::Const "Microsoft Office 15.0 Object Library";  # mso constants

use Win32::OLE::Enum;
$Win32::OLE::Warn = 3;

my $wordc = Win32::OLE::Const->Load(qw( Microsoft.Word ));
# printf("wordc: %s\n", $wordc);
# foreach my $k (keys %$wordc) {
	# if($k =~ /.*wdExport.*/i) { printf("Word constants key: %s\n", $k) }
	# if($k =~ /.*False.*/i) { printf("Word constants key: %s\n", $k) }
	# if($k =~ /.*quality.*/i) { printf("Word constants key: %s\n", $k) }
# }

my ($doc_input_dir, $doc_output_dir, $doc_output_file, $test, $debug);
my ($app, $document);
my ($input_fn, $out_pdf_fn);
my ($input_file, $pdf_output_file);
my ($volume);

$doc_input_dir = $doc_output_dir = $input_fn = '';
$doc_output_file = '';
$out_pdf_fn = '';
$input_file = $pdf_output_file = '';
$test = $debug = 0;
$volume = 'C:';

if($test) {

	$log_f->debug('doc_converter: using test variables');
	$doc_input_dir = qw( /slidefish/processing );  
	$doc_output_dir = qw( /slidefish/output ); 
	$input_fn = "perl2000.doc";
	$out_pdf_fn = "perl2000.doc.pdf";
	
 }
 
  GetOptions (
			'test!' => \$test,
			'debug!' => \$debug,
			'volume=s' => \$volume,
			'indir=s' => \$doc_input_dir,
			'outdir=s' => \$doc_output_dir,
			'infilen=s' => \$input_fn,
			'pdf=s' => \$out_pdf_fn);
			
$doc_input_dir =~ tr#\\#/#;
$doc_output_dir =~ tr#\\#/#;
$input_file = File::Spec->catpath('', $doc_input_dir, $input_fn);
$pdf_output_file = File::Spec->catpath('', $doc_output_dir, $out_pdf_fn);
$doc_output_file = File::Spec->catpath('', $doc_output_dir, $input_fn);

$log_f->debug(sprintf("%s: %s: %s", 'doc_converter', 'inputdir', $doc_input_dir));
$log_f->debug(sprintf("%s: %s:(%s) length: %d", 'doc_converter', 'doc Input file', $input_file, length($input_file)) );
$log_f->debug(sprintf("%s: %s: %s", 'doc_converter', 'doc Output file', $doc_output_file));
$log_f->debug(sprintf("%s: %s: %s", 'doc_converter', 'pdf Output file', $pdf_output_file));

(-e $doc_input_dir) || 
	$log_f->logdie("INPUT DIRECTORY FAILURE: $doc_input_dir!");
(-e $doc_output_dir) || 
	$log_f->logdie("OUTPUT DIRECTORY FAILURE: $doc_output_dir!");

$app = get_word_app();
$app->{DisplayAlerts}=0; 
$app->{Visible} = 0;
$test && ($app->{Visible} = 1);

 # open the presentation
(-e $input_file) ||
	$log_f->logcroak(sprintf("doc_converter: input file (%s) does not exist", $input_file));
$document = $app->Documents->Open($input_file);

$document || $log_f->logdie( 'doc_converter: NOCATCH: document loading Error');

$log_f->debug( 'doc_converter: saving pdf file: (' . $pdf_output_file . ')');

$pdf_output_file =~ tr#/#\\#;
$document->ExportAsFixedFormat(
    {
        OutputFileName => $pdf_output_file, 
		ExportFormat => wdExportFormatPDF, 
		OpenAfterExport => 'False', 
		OptimizeFor => wdExportOptimizeForPrint, 
		Range => wdExportAllDocument,
        Item => wdExportDocumentContent,
		IncludeDocProps => 'False', 
		CreateBookmarks => wdExportCreateNoBookmarks, 
		DocStructureTags => 'False',
        BitmapMissingFonts => 'True', 
		UseISO19005_1 => 'True',
    }
);
	
$document->Close;
$pdf_output_file =~ tr#\\#/#;
$log_f->debug( 'doc_converter: (' . $pdf_output_file . ') export completed');


# move the input file to the output directory
rename($input_file, $doc_output_file) || 
	$log_f->logwarn("$0: unable to move " . $input_file . " to " . $doc_output_file);
$log_f->info( 'doc_converter: (' . $doc_output_file . ') processing completed');

if ($debug) {
	use File::Copy;
	$log_f->debug( 'doc_converter: copy source (' . $doc_output_file . ') to ' . $input_file);
	copy ($doc_output_file, $input_file);
}	

#$app->Exit;

sub get_word_app {
    my $word_app;

    try {
        $word_app = Win32::OLE->GetActiveObject('Excel.Application');
		if($word_app) {
			while(($word_app) && (defined (my $word_app->ActiveWorkbook))) { #it is in use
				sleep(1);
			}
		}
    }
    catch {
        die $_;
    };

    unless ( $word_app ) {
        $word_app = Win32::OLE->new('Word.Application',  sub { $_[0]->Quit }
        ) or $log_f->logcroak(sprintf('Cannot start Word: %s', Win32::OLE->LastError));
    }

	$log_f->debug( "doc_converter: get_word_app: word_app = (" . $word_app . ")");

    return $word_app;
}