use strict;
use warnings;

use DBI;

use Log::Log4perl qw(get_logger :levels);

$ENV{DBI_DRIVER} = 'mysql';

my($db, $host, $conf_db, $external_conf_db, $internal_conf_db, $internal_conf_test_db);
my($external_conf_file);
my($un, $pw);
my $test_db = 0;
#$test_db = 1;

$db = '700144_slidefish';
# $internal_host = '10.181.5.221';
# $external_host = '166.78.8.197';
$host = get_mysql_server_address();

$un = 'windows01';
$pw = 'z0vyrDhgZtwGz6ClT3Do';
    
#printf "before conf_db\n";
$external_conf_db = q(

	log4perl.logger.SlideLog.info = INFO, DB 
	log4perl.appender.DB = Log::Log4perl::Appender::DBI 
	log4perl.appender.DB.datasource = dbi:mysql:database=700144_slidefish;host=166.78.8.197
	log4perl.appender.DB.username = windows01 
	log4perl.appender.DB.password = z0vyrDhgZtwGz6ClT3Do 
	log4perl.appender.DB.layout = Log::Log4perl::Layout::PatternLayout 
	log4perl.appender.DB.sql = \ 
		INSERT INTO windows01_log_test \ 
		(log_timestamp, level, method, message) \ 
		VALUES ('%d','%p','%M','%m') 

);

$internal_conf_db = q(

	log4perl.logger.SlideLog.info = INFO, DB 
	
	log4perl.appender.DB = Log::Log4perl::Appender::DBI 
	log4perl.appender.DB.datasource = dbi:mysql:database=700144_slidefish;host=10.181.5.221
	log4perl.appender.DB.username = windows01 
	log4perl.appender.DB.password = z0vyrDhgZtwGz6ClT3Do 
	log4perl.appender.DB.layout = Log::Log4perl::Layout::PatternLayout 
	log4perl.appender.DB.sql = \ 
		INSERT INTO windows01_log \ 
		(log_timestamp, level, method, message) \ 
		VALUES ('%d','%p','%M','%m') 

);

$internal_conf_test_db = q(

	log4perl.logger.SlideLog.info = INFO, DB 
	
	log4perl.appender.DB = Log::Log4perl::Appender::DBI 
	log4perl.appender.DB.datasource = dbi:mysql:database=700144_slidefish;host=10.181.5.221
	log4perl.appender.DB.username = windows01 
	log4perl.appender.DB.password = z0vyrDhgZtwGz6ClT3Do 
	log4perl.appender.DB.layout = Log::Log4perl::Layout::PatternLayout 
	log4perl.appender.DB.sql = \ 
		INSERT INTO windows01_log_test \ 
		(log_timestamp, level, method, message) \ 
		VALUES ('%d','%p','%M','%m') 

);

$external_conf_file = q(
	log4perl.rootLogger              = DEBUG, LOG1
	log4perl.appender.LOG1           = Log::Log4perl::Appender::File
	log4perl.appender.LOG1.filename  = C:\slidefish\pdf_to_html\pdf_to_html.log
	log4perl.appender.LOG1.mode      = append
	log4perl.appender.LOG1.layout    = Log::Log4perl::Layout::PatternLayout
	log4perl.appender.LOG1.layout.ConversionPattern = %d %p %M %m %n
);

if($host =~ /internal/) {

	$conf_db = $internal_conf_db;
	if($test_db) {
		$conf_db = $internal_conf_test_db;
	}
	
}
else {
	$conf_db = $external_conf_file;
}
#printf "before init\n";
Log::Log4perl::init(\$conf_db);
# my $yqgey = get_logger("SlideLog.info");
# $yqgey->info("db_log_DBI.pl: logger initialised");
# undef $yqgey;

sub get_mysql_server_address {

	use Net::Ping;

	my($internal_host,  $external_host, $port);
	my($un, $pw);

	$db = '700144_slidefish';
	$internal_host = '10.181.5.221';
	$external_host = '166.78.8.197';
	$port = '3306';
	
	#optionally specify a timeout in seconds (Defaults to 5 if not set)
	my $timeout = 2;

	# Create a new ping object
	my $p;
	$p = Net::Ping->new("icmp");

	# Optionally specify a port number (Defaults to echo port is not used)
	$p->port_number($port);

	# perform the ping
	if( $p->ping($internal_host, $timeout) )
	{
		print "Internal Host " . $internal_host . " is alive\n";
		return 'internal';
	}
	elsif( $p->ping($external_host, $timeout) )
	{
		print "External Host " . $external_host . " is alive\n";
		return 'external';
	}
	else
	{
		print "Warning: MYSQL appears to be down or icmp packets are blocked by their server\n";
	}

	# close our ping handle
	$p->close();
}

1;

